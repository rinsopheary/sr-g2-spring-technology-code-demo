package com.example.demo;


import com.example.demo.containers.*;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {

//        ApplicationContext context = SpringApplication.run(DemoApplication.class, args);
//        Reading reading = context.getBean("reading", Reading.class);
//        Social social = context.getBean("social", Social.class);
//        reading.readBook();


        SpringApplication.run(DemoApplication.class, args);
    }

}
