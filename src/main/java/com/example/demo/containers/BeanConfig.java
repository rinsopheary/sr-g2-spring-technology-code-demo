package com.example.demo.containers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Configuration
public class BeanConfig {

    @Bean
    public Test test(){
        return new Test();
    }

    @Bean
    public Student student(){
        return new Student(1, "Sopheary");
    }

    @Bean
    public Science science(){
        return new Science();
    }

    @Bean
    public Social social(){
        return new Social();
    }


    @PostConstruct
    public void init(){
        System.out.println("Init bean");
    }
}
